## First Steps

The first thing you want to do with the migration is plan it. Do they have on-premises exchange? Are you going to deploy just a [*dirsync*](#dirsync) to Azure or are you going to deploy ADFS for [SSO](#SSO)? 

Good reads on the subject:
 - https://technet.microsoft.com/en-us/office/dn756393.aspx
 - https://technet.microsoft.com/en-us/library/hh852466.aspx
 -  https://support.office.com/en-us/article/Deployment-planning-checklist-for-Office-365-5fa4f6ef-35ad-4840-91c1-4834df3df5a0?ui=en-US&rs=en-US&ad=US
 -  (Haven't looked at this one, but it looks potentially handy) https://fto365siteprod.azurewebsites.net/


## If the Client has On-Premises Exchange

If a client has exchange mailboxes that you need to sync, there are a few checklist items that have to be taken care of first:

- [x]  Ensure that no mailboxes exceed 50GB in size and that no individual mail messages exceed 25MB. Any offending mail items will not sync, and mailboxes too large won't sync completely. [See here for some info on how to accomplish this.](https://newsignature.com/articles/office-365-migration-tip-how-to-find-email-messages-larger-than-25mb-across-all-mailboxes)
    - In order for this to work, the admin account needs FullAccess Permissions on all mailboxes. The Exchange Admin Shell can help us there: 
```posh
Get-MailboxDatabase | Add-ADPermission -user [AdminUsername] -AccessRights GenericAll
```

- [x] Work with the client to get the mailboxes trimmed down. The users themselves will need to take care of that, as we don't have that kind of access to their mailboxes from the backend.

- [x] Setup the domain in Office365. **DON'T CHANGE MX RECORDS YET, JUST GET IT VERIFIED.**
	- As part of the process it will prompt you to start the Mailbox migration. Choose between [Cutover](https://support.office.com/en-us/article/Perform-a-cutover-migration-of-email-to-Office-365-9496e93c-1e59-41a8-9bb3-6e8df0cd81b4) and [Staged](https://support.office.com/en-us/article/Perform-a-staged-migration-of-email-to-Office-365-83bc0b69-de47-4cc4-a57d-47e478e4894e) Migration, whichever you deemed appropriate in the planning stage. Cutover will likely be the most appropriate option if it's not a hybrid Exchange deployment. (Migration will take days, if not more than a week depending on how much data is being moved.)

- [x] When the syncing is done, check for any errors in the migration and make sure that any that do appear are erroneous. If the errors are not erroneous, resolve them. (**NOTE: DO NOT DELETE THE MIGRATION BATCH YET**)
- [x] If you are going to do a [*dirsync*](#dirsync) or deploy [SSO](#SSO), do so now. After that is done and tested working (more on those below), choose a Friday night to update the MX records. DNS can take up to 48 hours to propagate, so Friday night is best for this, and by leaving the batch there any emails that hit the exchange server will sync over. Syncing is done every 24 hours. Only delete the batch when the migration is successfully complete.
- [x] After changing the MX records, it's important to delete the autodiscovery on the exchange server. From the Exchange Management Shell:

```posh
Get-AutodiscoverVirtualDirectory | fl Name, Server, InternalUrl, Identity
Remove-AutodiscoverVirtualDirectory -identity <AutodiscoverIdentity>
```
Or if there is only one domain, you can be a bit more indiscriminate:
```posh
Remove-AutodiscoverVirtualDirectory -identity *
```

### QUICK NOTE ABOUT MAIL-ENABLED DISTRO GROUPS

Regardless of what the settings were in Exchange, if you *dirsync* or enable SSO, it's going to synchronize the AD attributes of the distribution groups and override the security settings. In particular, the cause for concern is that outside email addresses will not be able to email the distribution group. For example: sales@contoso.com will be considered an internal-only group after the sync, and will not be able to accept mail from any address outside of the contoso.com domain.

The way to fix this is through *adsiedit.msc*. Upon launching the program: 

* select *"ADSI Editor"* from the top of the tree on the lift of the window. Then open the "Action" menu at the top and select "Connect to".
* In the window that appears, type a name for the object you are about to make. The name is arbitrary, so name it something that you or another tech after you can easily recognize as the correct object.
* Select the second radio option called *"Select a well-known naming context"*. Everything else can stay default. 
* Now in the tree on the left you should see the object you just made. If you expand that tree, you should see the same tree structure as their AD. Find the OU that contains the Distro Group. When you click on the OU, the list of containers in the OU appear in the right pane.
* Right-click on the distro group and click *"Properties"*
* Now in that list, find *"msExchRequireAuthToSendTo"*, double-click on it, and choose false. Apply the changes you made. Do this for all distro groups that need to be able to have outside mail sent to them.