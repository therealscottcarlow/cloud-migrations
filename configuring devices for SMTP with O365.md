By mail-enabled devices, I mean things such as copiers, or even LOB applications that send mail.

The nitty gritty of this process will depend entirely on the device, but here are a few things that will help:

1. **Do not use SMTP.outlook.com**. This method requires an active and provisioned O365 user. No customer wants to pay for their copier to be able to spend emails if they don't have to, and they don't.
	1. Instead use the following address:"{userdomain-com}.mail.protection.outlook.com". Obviously exclude the curly braces. Also, no, that hyphen between the domain name and "com" was not a typo. Consider the following examples:
		1. contoso-com.mail.protection.outlook.com
		2. elhaynes-org.mail.protection.outlook.com
		3. arin-net.mail.protection.outlook.com
	4. I'm sure you can see the trend. This is actually the MX record for the domain, but Microsoft has set this address up also as a virtual SMTP server. Pretty clever, actually. No auth, no SSL/TLS. Just straight-up unmitigated SMTP. This opens up device compatibility.
5. Sending mail via the above method has a tendency to get mail messages sent to the junk folder. The best way to avoid this is to add the client's static IP to their [SPF record on their domain](https://technet.microsoft.com/en-us/library/dn789058(v=exchg.150).aspx).
6. You also want to add the devices email address to the approved sender's list in Office 365's [Spam settings](https://technet.microsoft.com/library/jj200684(v=exchg.150).aspx).

Now just give everything an hour or two to propagate across Office 365 and you should have mail-enabled devices able to send mail.