**IMPORTANT: If you are migrating on-premises exchange to Office 365, do that first! Cutover migrations are not possible when you have already done a dirsync. At that point, staged migration is your only option and that will take a very long time to complete. You have been warned.**

Dirsync (or possibly aadconnect. See below.) is the recommended migration method for full, non-hybrid deployments. Setting up a dirsync is relatively simple. When viewing the Active Users page in the Office 365 Admin portal for the client, there will be a series of links at the top of the page. They may look like this:

![](https://i.imgur.com/aTYIf8p.png)

The *"Active Directory Synchronization"* portion will have a link that says *"Set up"*. Follow those instructions. Before you actually run the sync, though, there are steps to take in order to prepare AD for the sync. Going through it all here would make this document monolithic, so I'll just throw up some links then go over some of the gotcha's you might encounter:

- https://blogs.office.com/2014/04/15/synchronizing-your-directory-with-office-365-is-easy/
- https://support.office.com/en-us/article/Prepare-to-provision-users-through-directory-synchronization-to-Office-365-01920974-9e6f-4331-a370-13aea4e82b3e

**NOTE: The DirSync Tool is (eventually someday) going to be replaced with the Azure AD Connect tool. Dirsync is still supported, but if you want to deploy the new tool, read this: https://azure.microsoft.com/en-us/documentation/articles/active-directory-aadconnect/**

The dirsync is going to catch a metric ton of stuff that you do not need on Azure. It will clutters the users and groups on Office 365 and make it a pain to find what you're looking for should you ever need to go through it. If you deployed the Dirsync tool, which is the only tool I can speak for, we are going to have to do some manual futzing. Unfortunately, though, there is no such thing as preemptively stopping the mess, as a full synchronization will happen on install, and an install needs to happen in order to exclude things. Yay Micro$oft!

## Cleaning up unnecessarily sync'd containers

The first bit is excluding OU's that don't contain necessary users and groups from syncs. We do this through miis.exe, and the process is described [here.](http://blogs.technet.com/b/praveenkumar/archive/2014/04/11/how-to-do-ou-based-filtering-in-office-365.aspx)

Despite that, though, there will also be stuff in the appropriate containers that you do not need or want in Office 365, like user accounts created by programs or vendors. For that we need to fire up ADSIedit. The process is the same as you may have read in [Migrating On-premises Exchange to Office 365](https://dnsolutions.itglue.com/DOC-41424-152059), but in case you didn't have to read that to get this far, I'll paste the relevant info:

- Run adsiedit.msc
- select "ADSI Editor" from the top of the tree on the lift of the window. Then open the "Action" menu at the top and select "Connect to".
- In the window that appears, type a name for the object you are about to make. The name is arbitrary, so name it something that you or another tech after you can easily recognize as the correct object.
- Select the second radio option called "Select a well-known naming context". Everything else can stay default.
- Now in the tree on the left you should see the object you just made. If you expand that tree, you should see the same tree structure as their AD. Find the OU that contains the container you want to exclude from the sync. When you click on the OU, the list of containers in the OU appear in the right pane.
- Right-click on the container and click "Properties"
- Now in that list, find "**extensionAttribute15**", double-click on it, and set the value to "**NoSync**". Apply the changes you made. Do this for all containers you want to exclude from the sync.

### Telling the dirsync program to ignore containers that have **extensionAttribute15** set to **NoSync**.

- Log on to the computer that is running directory synchronization by using an account that is a member of the MIISAdmins local security group.
- Open Identity Manager by double-clicking miisclient.exe that is located in the following folder:
	- %ProgramFiles%\Windows Azure Active Directory Sync\SYNCBUS\Synchronization Service\UIShell

- In Identity Manager, click Management Agents, and then double-click Active Directory Connector.
- Click Configure Connector Filter, and then do the following:
	- Select user in the Data Source Object Type grid, and then click New.
	- In Filter for user, on the Data Source attribute, select extensionAttribute15; for Operator, select Equals, and then type NoSync in the Value field.
	- Click Add Condition, and then click OK.

- On the SourceAD properties page, click OK.
- Perform a full sync: on the Management Agent tab, right-click Active Directory Connector, click Run, click Full Import Full Sync, and then click OK.

Now you should have AD Sync'd and the users/groups in Office365 should be clear of clutter, making for easier management. Give the changes an hour or two to propagate across the Office 365 service. After that is done, activate the users that are getting Office 365 licenses, as described in some of the above reading. Office 365 will automaticall associate the user with their mailbox.